#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <SPI.h>
#include <Wire.h>
#include <RTClib.h>
#include <IRremote.h>
#include <NewTone.h>

#include "AlarmClock.h"
//#include "save.h"

#define TFT_CS 10
#define TFT_RST 8  
#define TFT_DC 9
AlarmClock* alarm;

//int melody[] = {NOTE_C4, NOTE_G3, NOTE_G3, NOTE_A3, NOTE_G3, 0, NOTE_B3, NOTE_C4};
// note durations: 4 = quarter note, 8 = eighth note, etc.:
//int noteDurations[] = {4, 8, 8, 4, 4, 4, 4, 4};

void setup() {
  Serial.begin(9600);
  alarm = new AlarmClock(TFT_CS, TFT_DC, TFT_RST, 3, 6);
}

void loop() {
  alarm->printCurrentTime();
  alarm->shouldAnAlarmGoOff();
  alarm->isMenuAskedFor();
}
