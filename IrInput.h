#include <IRremote.h>
#include "IRRemoteButtons.h"

class IrInput{
  private:
    IRrecv* reciever;
    long lastInputReceived;

  public:
    IrInput(int irRecieverPin);
    long getButtonPushed();
    bool inactiveForTime(int millisec);
};
