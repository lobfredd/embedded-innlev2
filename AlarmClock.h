#include <RTClib.h>
#include <Adafruit_ST7735.h>
#include <Wire.h>
#include "IrInput.h"
#include "Eeprom.h"
#include "AlarmSound.h"
#define ALARM_COUNT 20
#define MENU_TIMEOUT 20000

typedef struct{
    long lastFlash;
    int flashTime;
    bool visible;
  }FlashData;

typedef struct{
  DateTime time;
  bool on;
}Alarm;

class AlarmClock{
  private: 
    Adafruit_ST7735* tft;
    RTC_DS1307* rtc;
    DateTime lastCurrentTimePrint;
    IrInput* input;
    Eeprom* eeprom;
    Alarm alarms[ALARM_COUNT];
    AlarmSound* outSound;
    Alarm* firstUpcomingAlarm;
    short rotation;
    short tftWidth;
    short tftHeight;
    
  public:
    AlarmClock(int tft_cs, int tft_dc, int tft_rst, int ir_pin, int sound_pin);
    void printCurrentTime();
    void printAlarmTime();
    void isMenuAskedFor();
    void shouldAnAlarmGoOff();

  private:
    void setRotation(short newRotation);
    void rotate();
    void loadAlarms();
    void saveAlarms();
    void initAlarms();
    
    void drawDateTime(DateTime time);
    void drawTime(uint8_t time);
    
    void showMenu();
    int drawMenu(int currentPos);
    void moveMenuCursor(int hPos, int oldPos, int newPos);
    void manageAlarms();
    int drawAlarms(int startIndex);
    void editAlarm(int alarmIndex);
    int drawEditAlarmMenu(int alarmIndex);
    void adjustClock();
    DateTime adjustDateTime(DateTime time, String title);
    void flipFlash(FlashData* flash);
    void flashClockSelector(DateTime now, bool hourOrMinute, FlashData* flash);
    void invisibleOrNot(uint8_t time, bool invisible);
    void reDrawTextAt(int x, int y, String oldValue, String newValue);
    TimeSpan calculateTimeTo(DateTime to, DateTime from);
    void drawAdjustDateTime(String title);
};

