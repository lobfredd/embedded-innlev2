#include "AlarmClock.h"

void AlarmClock::reDrawTextAt(int x, int y, String oldValue, String newValue){
  if(oldValue.length() > 0){
    tft->setTextColor(ST7735_BLACK);
    tft->setCursor(x, y);
    tft->print(oldValue);
  }
  if(newValue.length() > 0){
    tft->setTextColor(ST7735_WHITE);
    tft->setCursor(x, y);
    tft->print(newValue);
  }
}

int AlarmClock::drawMenu(int currentPos){
  int width = 8*6*2;
  int hPos = (tftWidth-width)/2;
  tft->fillScreen(ST7735_BLACK);
  tft->setTextSize(2);
  tft->setCursor(hPos, 10);
  tft->print("Settings");

  int txtSize = tftWidth/(6*15);
  width = 6*15*txtSize;
  hPos = (tftWidth-width)/2;
  tft->setTextSize(txtSize);
  tft->setTextColor(ST7735_WHITE);
  tft->setCursor(hPos, 40);
  tft->print("Manage alarms");
  
  tft->setCursor(hPos, 60);
  tft->print("Adjust clock");
  
  tft->setCursor(hPos, 80);
  tft->print("Rotate Screen");

  moveMenuCursor(hPos, currentPos, currentPos);
  return hPos;
}

int AlarmClock::drawEditAlarmMenu(int alarmIndex){
  int width = 8*6*2;
  int hPos = (tftWidth-width)/2;
  tft->fillScreen(ST7735_BLACK);
  tft->setTextSize(2);
  tft->setCursor(hPos, 20);
  tft->print("Alarm: ");
  tft->print(alarmIndex+1);
  
  width = 11*6;
  hPos = (tftWidth-width)/2;
  tft->setTextSize(1);
  tft->setCursor(hPos, 40);
  tft->print("Time: ");
  drawDateTime(alarms[alarmIndex].time);

  
  tft->setCursor(hPos, 60);
  tft->print("Edit time");
  tft->setCursor(hPos, 70);
  tft->print("Status: ");
  tft->print(alarms[alarmIndex].on ? "on" : "off");
  return hPos;
}

void AlarmClock::drawDateTime(DateTime time){
  drawTime(time.hour());
  tft->print(':');
  drawTime(time.minute());
}

void AlarmClock::drawTime(uint8_t time){
  if(time < 10) tft->print('0');
   tft->print(time);
}

int AlarmClock::drawAlarms(int startIndex){
  int width = 6*6*2;
  int hPos = (tftWidth-width)/2;
  tft->fillScreen(ST7735_BLACK);
  tft->setTextSize(2);
  tft->setCursor(hPos, 10);
  tft->print("Alarms");

  width = 6*12;
  hPos = (tftWidth-width)/2;
  tft->setTextSize(1);
  tft->fillRect(0, 40, tftWidth, tftHeight-40, ST7735_BLACK);
  int secondI = 0;
  for(int i = startIndex; i < ALARM_COUNT; i++){
   tft->setCursor(hPos, secondI*8+40);
   tft->print(i+1);
   if(i < 9) tft->print(":  ");
   else tft->print(": ");
   drawDateTime(alarms[i].time);
   if(alarms[i].on) tft->print(" on");
   secondI++;
  }
  return hPos;
}

void AlarmClock::drawAdjustDateTime(String title){
  int width = 6 * title.length();
  int hPos = (tftWidth - width)/2;
  tft->fillScreen(ST7735_BLACK);
  tft->setCursor(hPos, 20);
  tft->setTextSize(1);
  tft->print(title);
  tft->setCursor(0, tftHeight-8);
  tft->print("* to quit");
  tft->setCursor(tftWidth-9*6, tftHeight-8);
  tft->print("# to save");
}

