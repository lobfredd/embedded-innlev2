#include "Eeprom.h"

void Eeprom::save(byte* bytes, int byteCount, byte msbAddress, byte lsbAddress){
  Wire.begin();
  delay(100);
  Serial.println("is save");
  int parts = (byteCount % 30 == 0) ? byteCount / 30 : (byteCount/30) + 1;
  int retries = 0;

  for(int i = 0; i < parts; i++){
    int currIndex = i*32;
    Wire.beginTransmission(0x50);
    Wire.write(msbAddress);
    Wire.write(currIndex + lsbAddress);
    for(int j = i*30; j < ((i*30)+30) && j < byteCount; j++){
      Wire.write(bytes[j]);
    }
    delay(5);
    byte error = Wire.endTransmission();
    if(error != 0 && retries++ < 10) i--; //try again;
  }
}

void Eeprom::load(byte* dest, int byteCount, byte msbAddress, byte lsbAddress){
  Wire.begin();
  delay(100);
  Serial.println("in load");
  int bytesRetrieved = 0;
  int parts = (byteCount % 30 == 0) ? byteCount / 30 : (byteCount/30)+1;
 
  for(int i = 0; i < parts; i++){
    int currIndex = i*32;
    Wire.beginTransmission(0x50);
    Wire.write(msbAddress);
    Wire.write(currIndex + lsbAddress);
    Wire.endTransmission();
    int bytesToRead = ((i*30) + 30 < byteCount) ? 30 : byteCount-(i*30);
    Wire.requestFrom(0x50, bytesToRead);
    while(Wire.available()){
      dest[bytesRetrieved++] = Wire.read();
    }
  }
}
