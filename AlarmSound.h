#include <Arduino.h>
#include "pitches.h"
#include <NewTone.h>

#define NOTE_COUNT 8

class AlarmSound{
  private:
    int outputPin;
    int melody[NOTE_COUNT] = {NOTE_C4, NOTE_G3, NOTE_G3, NOTE_A3, NOTE_G3, 0, NOTE_B3, NOTE_C4};
    int noteDurations[NOTE_COUNT] = {4, 8, 8, 4, 4, 4, 4, 4};
    int lastPlayedTone;

  public:
    AlarmSound(int outputPin);
    void playNextTone();
    
};

