#include "AlarmSound.h"

AlarmSound::AlarmSound(int outputPin){
  this->outputPin = outputPin;
  pinMode(outputPin, OUTPUT);
  lastPlayedTone = 0;
}

void AlarmSound::playNextTone(){
    int noteDuration = 1000 / noteDurations[lastPlayedTone];
    NewTone(6, melody[lastPlayedTone], noteDuration);
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    noNewTone(6);
    if(lastPlayedTone < NOTE_COUNT-1) lastPlayedTone++;
    else lastPlayedTone = 0;
}

