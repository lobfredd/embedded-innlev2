#include "AlarmTFT.h"

AlarmTFT::AlarmTFT(int tft_cs, int tft_dc, int tft_rst) 
  : Adafruit_ST7735(tft_cs, tft_dc, tft_rst){
    
  }

void AlarmTFT::drawMenu(int currentPos, bool alarmOn){
  fillScreen(ST7735_BLACK);
  setTextSize(1);
  setTextColor(ST7735_WHITE);
  setCursor(30, 20);
  print("Alarm on: ");
  print(alarmOn ? "true" : "false");

  setCursor(30, 40);
  print("Adjust clock");
   
  setCursor(30, 60);
  print("Rotate Screen");


  moveMenuCursor(20, currentPos);
}

void AlarmTFT::moveMenuCursor(int oldPos, int newPos){
  reDrawTextAt(20, oldPos, "*", "");
  reDrawTextAt(20, newPos, "", "*");
}

void AlarmTFT::reDrawTextAt(int x, int y,String oldValue, String newValue){
  if(oldValue.length() > 0){
    setTextColor(ST7735_BLACK);
    setCursor(x, y);
    print(oldValue);
  }
  if(newValue.length() > 0){
    setTextColor(ST7735_WHITE);
    setCursor(x, y);
    print(newValue);
  }
}

