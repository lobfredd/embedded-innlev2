#include <Arduino.h>
#include <Wire.h>

class Eeprom{
  public:
    void save(byte* bytes, int byteCount, byte msbAddress, byte lsbAddress);
    void load(byte* dest, int byteCount, byte msbAddress, byte lsbAddress);
};


