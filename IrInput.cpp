#include "IrInput.h"

IrInput::IrInput(int irRecieverPin){
  this->reciever = new IRrecv(irRecieverPin);
  reciever->enableIRIn();
  lastInputReceived = 0;
}

long IrInput::getButtonPushed(){
  decode_results results;
  if(reciever->decode(&results)) {
    lastInputReceived = millis();
    reciever->resume(); // Receive the next value
    return results.value;
  }
  return -1;
}

bool IrInput::inactiveForTime(int millisec){
  return millis() - lastInputReceived > millisec;
}
