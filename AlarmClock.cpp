#include "AlarmClock.h"

AlarmClock::AlarmClock(int tft_cs, int tft_dc, int tft_rst, int ir_pin, int soundpin){
  tft = new Adafruit_ST7735(tft_cs, tft_dc, tft_rst);
  rtc = new RTC_DS1307;
  eeprom = new Eeprom();
  outSound = new AlarmSound(soundpin);
  input = new IrInput(ir_pin);
  tft->initR(INITR_BLACKTAB);
  tft->fillScreen(ST7735_BLACK);
  setRotation(3);
  rtc->begin();
  loadAlarms();
  firstUpcomingAlarm = new Alarm{DateTime(0), false};
  printAlarmTime();
}

void AlarmClock::saveAlarms(){
  int totBytes = sizeof(Alarm) * ALARM_COUNT;
  eeprom->save((byte*)alarms, totBytes, 0, 0);
}

void AlarmClock::loadAlarms(){
  int totBytes = sizeof(Alarm) * ALARM_COUNT;
  byte dest[totBytes];
  eeprom->load(dest, totBytes, 0,0); 
  Alarm* newAlarms = (Alarm*)dest;
  
  for(int i = 0; i < ALARM_COUNT; i++){
    if(newAlarms[i].time.hour() > 23 || newAlarms[i].time.minute() > 59 ||
      (newAlarms[i].on != 1 && newAlarms[i].on != 0)){ //Alarms currupted or never actually saved here.
        initAlarms();
        saveAlarms();
        break;
      }
    alarms[i] = newAlarms[i];
  }
}

void AlarmClock::initAlarms(){
  for(int i = 0; i < ALARM_COUNT; i++){
    alarms[i] = {DateTime(0), false};
  }
}

void AlarmClock::printCurrentTime(){
  DateTime now = rtc->now();
  if(now.hour() == lastCurrentTimePrint.hour() && 
      now.minute() == lastCurrentTimePrint.minute()) return;

  int txtSize = tftWidth/(6*5);
  int width = 6*5*txtSize;
  int height = 7*txtSize;
  int hPos = (tftWidth-width)/2 + (txtSize/2);
  int vPos = (tftHeight-height)/2;
  tft->fillRect(hPos, vPos, width, height, ST7735_BLACK);
  tft->setCursor(hPos, vPos);
  tft->setTextSize(txtSize);
  tft->setTextColor(ST7735_WHITE);
  
  drawDateTime(now);
  lastCurrentTimePrint = now;
}

void AlarmClock::printAlarmTime(){
  int width = 6*5;
  int hPos = tftWidth - width;
  tft->fillRect(hPos, 0, width, 8, ST7735_BLACK);
  tft->setCursor(hPos, 0);
  tft->setTextSize(1);
  tft->setTextColor(ST7735_WHITE);

  DateTime now = rtc->now();
  DateTime firstUpcomingTmp = DateTime(0,0,0, now.hour(), now.minute()-1, 0); //worst possible
  bool alarmIsOn = false;
  int indexFirstUpcoming = 0;
  for(int i = 0; i < ALARM_COUNT; i++){
    if(alarms[i].on){
      alarmIsOn = true;
      TimeSpan a = calculateTimeTo(firstUpcomingTmp, now);
      TimeSpan b = calculateTimeTo(alarms[i].time, now);
      if(b.totalseconds() < a.totalseconds()){
        firstUpcomingTmp = alarms[i].time;
        indexFirstUpcoming = i;
      }
    }
  }
  if(alarmIsOn){
    firstUpcomingAlarm = &alarms[indexFirstUpcoming];
    drawDateTime(firstUpcomingAlarm->time);
  }
}

void AlarmClock::isMenuAskedFor(){
  long btnPush = input->getButtonPushed();
  if(btnPush == BTN_POUND) showMenu();
}

void AlarmClock::shouldAnAlarmGoOff(){
  DateTime now = rtc->now();
  if(!(firstUpcomingAlarm->on && 
     now.hour() == firstUpcomingAlarm->time.hour() && 
     now.minute() == firstUpcomingAlarm->time.minute())) return; //notice the ! wrapped around the whole test

  long lastInvert = millis();
  bool invertToggle = false;
  long btnPush;
  while((btnPush = input->getButtonPushed()) != BTN_STAR){
    printCurrentTime(); 
    outSound->playNextTone();
    if(btnPush == BTN_OK){
      TimeSpan snoozeTime = TimeSpan(0,0,10,0);
      firstUpcomingAlarm->time = DateTime(firstUpcomingAlarm->time + snoozeTime);
      return;
    }
    if(millis() - lastInvert > 1000){
      lastInvert = millis();
      tft->invertDisplay(invertToggle);
      invertToggle = !invertToggle;
    }
  }
  tft->invertDisplay(false);
  firstUpcomingAlarm->on = false;
  printAlarmTime();
}

void AlarmClock::setRotation(short newRotation){
  rotation = newRotation;
  tft->setRotation(rotation);
  if(rotation == 1 || rotation == 3){
    tftWidth = ST7735_TFTHEIGHT_18;
    tftHeight = ST7735_TFTWIDTH;
  }
  else {
    tftWidth = ST7735_TFTWIDTH;
    tftHeight = ST7735_TFTHEIGHT_18;
  }
}
void AlarmClock::rotate(){
  if(rotation == 3) rotation = 0;
  else rotation++;
  setRotation(rotation);
}



TimeSpan AlarmClock::calculateTimeTo(DateTime to, DateTime from){
  int hoursTo = to.hour() - from.hour();
  int minsTo = to.minute() - from.minute();
  if(to.hour() == from.hour() && to.minute() < from.minute()) hoursTo--; //same hour, some minutes ago;
  if(hoursTo < 0) hoursTo += 24;
  if(minsTo < 0) minsTo += 60;
  return TimeSpan(0, hoursTo, minsTo, 0);
}



void AlarmClock::showMenu(){
  int currentPos = 40;
  
  int hPos = drawMenu(currentPos);
  long btnPush;
  while((btnPush = input->getButtonPushed()) != BTN_STAR && !input->inactiveForTime(MENU_TIMEOUT)){
    if(btnPush == BTN_OK && currentPos == 40){
      //toggleAlarmStatus();
      manageAlarms();
      hPos = drawMenu(currentPos);
    }
    else if(btnPush == BTN_OK && currentPos == 60){
      adjustClock();
      drawMenu(currentPos);
    }
    else if(btnPush == BTN_OK && currentPos == 80){
      rotate();
      hPos = drawMenu(currentPos); //new hPos needed because rotation
    }
    else if(btnPush == ARROW_DOWN && currentPos < 80){
      currentPos += 20;
      moveMenuCursor(hPos, currentPos-20, currentPos);
    }
    else if(btnPush == ARROW_UP && currentPos > 40){
      currentPos -= 20;
      moveMenuCursor(hPos, currentPos + 20, currentPos);
    }
  }
  tft->fillScreen(ST7735_BLACK);
  printAlarmTime();
  lastCurrentTimePrint = DateTime(0); //forcing reprint of clock
}

void AlarmClock::manageAlarms(){
  int firstLinePos = 40;
  int currentPos = firstLinePos;
  int screenBottom = tftHeight-8;
  int lines = (tftHeight-firstLinePos)/8;
  int currentSelection = 0;
  int hPos = drawAlarms(currentSelection);
  moveMenuCursor(hPos, currentPos, currentPos);
  
  long btnPush;
  while((btnPush = input->getButtonPushed()) != BTN_STAR && !input->inactiveForTime(MENU_TIMEOUT)){
    if(btnPush == BTN_OK){
      editAlarm(currentSelection);
      if(currentSelection >= lines){
        drawAlarms(currentSelection-lines+1);
        currentPos = screenBottom;
      }
      else{
        currentPos = currentSelection * 8 + firstLinePos;
        drawAlarms(0);
      }
      moveMenuCursor(hPos, currentPos, currentPos);
    }
     if(btnPush == ARROW_DOWN && currentSelection < ALARM_COUNT-1){
      if(currentPos < screenBottom) currentPos += 8;
      currentSelection++;
      if(currentPos >= screenBottom) drawAlarms(currentSelection-lines+1);
      moveMenuCursor(hPos, currentPos - 8, currentPos);
    }
    else if(btnPush == ARROW_UP && currentSelection > 0){
      if(currentPos > firstLinePos)currentPos -= 8;
      currentSelection--;
      if(currentPos <= firstLinePos) drawAlarms(currentSelection);
      moveMenuCursor(hPos, currentPos + 8, currentPos);
    }
  }
}

void AlarmClock::editAlarm(int alarmIndex){
  int hPos = drawEditAlarmMenu(alarmIndex);
  int currentPos = 60;
  moveMenuCursor(hPos, currentPos - 10, currentPos);
  long btnPush;
  while((btnPush = input->getButtonPushed()) != BTN_STAR && !input->inactiveForTime(MENU_TIMEOUT)){
    if(btnPush == BTN_OK && currentPos == 60){
      DateTime newTime = adjustDateTime(alarms[alarmIndex].time, "Set the new time..");
      if(newTime.unixtime() != 0){
        alarms[alarmIndex].time = newTime;
        saveAlarms();
      }
      drawEditAlarmMenu(alarmIndex);
      moveMenuCursor(hPos, currentPos - 10, currentPos);
    }
    else if(btnPush == BTN_OK && currentPos == 70){
      String oldVal = alarms[alarmIndex].on ? "on" : "off";
      alarms[alarmIndex].on = !(alarms[alarmIndex].on);
      String newVal = alarms[alarmIndex].on ? "on" : "off";
      reDrawTextAt(hPos+8*6, 70, oldVal, newVal);
      saveAlarms();
    }
    else if(btnPush == ARROW_DOWN && currentPos < 70){
      currentPos += 10;
      moveMenuCursor(hPos, currentPos - 10, currentPos);
    }
    else if(btnPush == ARROW_UP && currentPos > 60){
      currentPos -= 10;
      moveMenuCursor(hPos, currentPos + 10, currentPos);
    }
  } 
}

void AlarmClock::adjustClock(){
  DateTime time = adjustDateTime(rtc->now(), "Set the new time..");
  if(time.unixtime() != 0) rtc->adjust(time);
}

DateTime AlarmClock::adjustDateTime(DateTime time, String title){ 
  int txtSize = tftWidth / (6 * 5);
  int width = 6 * 5 * txtSize;
  int height = 7 * txtSize;
  int hPos = (tftWidth - width) / 2 + (txtSize/2);
  int vPos = (tftHeight - height) / 2;
  drawAdjustDateTime(title);
  tft->setTextSize(txtSize);
  
  bool hourOrMinute = true;
  long btnPush;
  FlashData flash = {.lastFlash = millis(), .flashTime = 100, .visible = true};
  
  while((btnPush = input->getButtonPushed()) != BTN_STAR && !input->inactiveForTime(MENU_TIMEOUT)){
    tft->setCursor(hPos, vPos);
    TimeSpan diff = hourOrMinute ? TimeSpan(0, 1, 0, 0) : TimeSpan(0, 0, 1, 0);
    
    flashClockSelector(time, hourOrMinute, &flash);
    if(btnPush == ARROW_UP){
      time = DateTime(time + diff);
      tft->fillRect(hPos, vPos, width, height, ST7735_BLACK);
    }
    else if(btnPush == ARROW_DOWN){
      time = DateTime(time - diff);
      tft->fillRect(hPos, vPos, width, height, ST7735_BLACK);
    }
    else if(btnPush == BTN_OK || btnPush == ARROW_LEFT || btnPush == ARROW_RIGHT) hourOrMinute = !hourOrMinute;
    else if(btnPush == BTN_POUND) return time;
  }
  return DateTime(0);
}

void AlarmClock::flashClockSelector(DateTime now, bool hourOrMinute, FlashData* flash){
  flipFlash(flash);
  if(!hourOrMinute){
    tft->setTextColor(ST7735_WHITE);
    drawTime(now.hour());
    tft->print(":");
    invisibleOrNot(now.minute(), flash->visible);
  }
  else{
    invisibleOrNot(now.hour(), flash->visible);
    tft->setTextColor(ST7735_WHITE);
    tft->print(":");
    drawTime(now.minute());
  }
}

void AlarmClock::invisibleOrNot(uint8_t time, bool invisible){
  if(invisible)tft->setTextColor(ST7735_BLACK);
  else tft->setTextColor(ST7735_WHITE);
  drawTime(time);
}

void AlarmClock::flipFlash(FlashData* flash){
  if(millis() - flash->lastFlash > flash->flashTime){
      flash->visible = !flash->visible;
      flash->lastFlash = millis();
      flash->flashTime = flash->visible ? 100 : 700;
    }
}

void AlarmClock::moveMenuCursor(int hPos, int oldPos, int newPos){
  reDrawTextAt(hPos-10, oldPos, "*", "");
  reDrawTextAt(hPos-10, newPos, "", "*");
}



