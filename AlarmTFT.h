#include <Adafruit_ST7735.h>

class AlarmTFT : public Adafruit_ST7735{
  public:
    AlarmTFT(int tft_cs, int tft_dc, int tft_rst);
    void drawMenu(int currentPos, bool alarmOn);
    void moveMenuCursor(int oldPos, int newPos);
    void reDrawTextAt(int x, int y, String oldValue, String newValue);
};

